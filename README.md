# 创建Kotlin应用程序
## 创建一个新的项目
打开Android Studio，选择Projects>New Project，然后选择Basic Activity.
![图片](Image/1/图片18.png)
### 创建模拟器
![图片](Image/1/图片19.png)
### 在模拟器上运行应用程序
## 更改布局约束
### 更改TextView
打开 values/strings.xml
修改字符串属性值为“Hello Kotlin!”。修改字体显示属性，在Design视图中选择textview_first文本组件，在Common Attributes属性下的textAppearance域，设置相关的文字显示属性
更改之后查看显示效果

![图片](Image/1/图片1.png)

## 添加按钮和约束
### 更改Next_Button
1.Next按钮是工程创建时默认的按钮，查看Next按钮的布局设计视图，它与TextView之间的连接不是锯齿状的而是波浪状的，表明两者之间存在链（chain），是一种两个组件之间的双向联系而不是单向联系。删除两者之间的链，可以在设计视图右键相应约束，选择Delete
2.在属性面板中更改Next按钮的id，从button_first改为random_button。在string.xml文件，右键next字符串资源，选择 Refactor > Rename，修改资源名称为random_button_text，点击Refactor 。随后，修改Next值为Random。
![图片](Image/1/图片2.png)
![图片](Image/1/图片6.png)
### 添加Totast_Button
调整Button的约束，设置Button的Top>BottonOf textView
```
app:layout_constraintTop_toBottomOf="@+id/textview_first" />
```
随后添加Button的左侧约束至屏幕的左侧，Button的底部约束至屏幕的底部。查看Attributes面板，修改将id从button修改为toast_button.

### 添加新的约束
添加Next的右边和底部约束至父类屏幕（如果不存在的话），Next的Top约束至TextView的底部。最后，TextView的底部约束至屏幕的底部。
效果看起来如下图所示：

![图片](Image/1/图片3.png)

### 更改组件的文本
fragment_first.xml布局文件代码中，找到toast_button按钮的text属性部分

![图片](Image/1/图片4.png)

将text的映射改为toast_button_text，资源值为Toast，并点击OK

![图片](Image/1/图片5.png)
### 添加第三个按钮
向fragment_first.xml文件中添加第三个按钮，位于Toast和Random按钮之间，TextView的下方。新Button的左右约束分别约束至Toast和Random，Top约束至TextView的底部，Buttom约束至屏幕的底部，显示效果：

![图片](Image/1/图片7.png)

更改新增按钮id为count_button，显示字符串为Count，对应字符串资源值为count_button_text。
### 更新文本框的外观
在Color.xml中定义两种颜色
```
<color name="screenBackground">#2196F3</color>
<color name="buttonBackground">#BBDEFB</color>
```
fragment_first.xml的属性面板中设置屏幕背景色为

```android:background="@color/screenBackground"```

设置每个按钮的背景色为buttonBackground

```android:background="@color/buttonBackground"```

最终效果如下图：

![图片](Image/1/图片8.png)

### 设置组件位置
Toast与屏幕的左边距设置为24dp

![图片](Image/1/图片9.png)

TextView的垂直偏移为0.3

![图片](Image/1/图片10.png)
## 运行应用程序
最终效果如下图：

![图片](Image/1/图片11.png)
## 添加代码完成应用程序交互
### 设置代码自动补全
Android Studio中，依次点击File>New Projects Settings>Settings for New Projects…，查找Auto Import选项，在Java和Kotlin部分，勾选Add Unambiguous Imports on the fly。
![图片](Image/1/图片12.png)
### 添加toast按钮消息事件
```
binding.randomButton.setOnClickListener {
    findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
}

// find the toast_button by its ID and set a click listener
view.findViewById<Button>(R.id.toast_button).setOnClickListener {
   // create a Toast with some text, to appear for a short time
   val myToast = Toast.makeText(context, "Hello Toast!", Toast.LENGTH_LONG)
   // show the Toast
   myToast.show()
}
```
### 添加count按钮响应事件
```
view.findViewById<Button>(R.id.count_button).setOnClickListener {
   countMe(view)
}

private fun countMe(view: View) {
   // Get the text view
   val showCountTextView = view.findViewById<TextView>(R.id.textview_first)

   // Get the value of the text view.
   val countString = showCountTextView.text.toString()

   // Convert value to a number and increment it
   var count = countString.toInt()
   count++

   // Display the new value in the text view.
   showCountTextView.text = count.toString()
}

```
## fragmentSecond代码实现
### 向界面添加TextView显示随机数
1. 打开fragment_second.xml的设计视图中，当前界面有两个组件，一个Button和一个TextView（textview_second）。
2. 去掉TextView和Button之间的约束
3. 拖动新的TextView至屏幕的中间位置，用来显示随机数
4. 设置新的TextView的id为**@+id/textview_random**
5. 设置新的TextView的左右约束至屏幕的左右侧，Top约束至textview_second的Bottom，Bottom约束至Button的Top
6. 设置TextView的字体颜色textColor属性为**@android:color/white**，textSize为72sp，textStyle为bold
7. 设置TextView的显示文字为“R”
8. 设置垂直偏移量layout_constraintVertical_bias为0.45

TextView最终的属性代码为：

![图片](Image/1/图片13.png)

textview_second最终属性代码为：

![图片](Image/1/图片14.png)

### 更改界面的背景色和按钮布局
最终效果如下图：

![图片](Image/1/图片15.png)

### 使用导航图和启用SafeArgs

点击nav_graph.xml文件可以查看导航图

SafeArgs 是一个 gradle 插件，它可以帮助您在导航图中输入需要传递的数据信息，作用类似于Activity之间传递数据的Bundle。

Gradle的Project部分，在plugins节添加

```id 'androidx.navigation.safeargs.kotlin' version '2.5.0-alpha01' apply false```

module部分在plugins节添加并且重新build

```id 'androidx.navigation.safeargs'```

### 创建导航动作的参数
1. 打开导航视图，点击FirstFragment，查看其属性。
2. 在Actions栏中可以看到导航至SecondFragment
3. 同理，查看SecondFragment的属性栏
4. 点击Arguments
5. 弹出的对话框中，添加参数myArg，类型为整型Integer

### 两个页面进行数据交互
1. 注释掉原先的事件处理代码
2. 实例化TextView，获取TextView中文本并转换为整数值
3. 将currentCount作为参数传递给actionFirstFragmentToSecondFragment()

![image.png](Image/1/图片20.png)

## 添加SecondFragment的代码
1. onViewCreated()中获取传递过来的参数列表，提取count数值，并在textview_header中显示
2. 根据count值生成随机数,textview_random中显示count值

![image.png](Image/1/图片21.png)

## 最终效果如图:
![图片](Image/1/图片16.png)
